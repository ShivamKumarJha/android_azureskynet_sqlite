package shivam.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeleteHome extends AppCompatActivity {

    EditText e;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_home);

        e = (EditText) findViewById(R.id.e4);
        b = (Button) findViewById(R.id.b4);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHome d = new DBHome(DeleteHome.this);
                d.deleteData(Integer.parseInt(e.getText().toString()));
                Toast.makeText(DeleteHome.this, "Deleted records", Toast.LENGTH_SHORT).show();
                d.close();
            }
        });
    }
}

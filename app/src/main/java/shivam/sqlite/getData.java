package shivam.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class getData extends AppCompatActivity {
    ListView l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_data);
        l = (ListView) findViewById(R.id.l1);
        final DBHome d = new DBHome(getData.this);
        ArrayAdapter name = new ArrayAdapter(getData.this,android.R.layout.simple_list_item_1,d.getData().get("sname"));
        l.setAdapter(name);
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getData.this, "\nNo\t"+d.getData().get("sno").get(position)
                        +"\nAge\t"+d.getData().get("sage").get(position), Toast.LENGTH_SHORT).show();
            }
        });
        d.close();
    }
}

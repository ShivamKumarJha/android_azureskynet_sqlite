package shivam.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateHome extends AppCompatActivity {

    EditText e1,e2,e3;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_home);
        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        e3 = (EditText) findViewById(R.id.e3);
        b = (Button) findViewById(R.id.b);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHome dbh = new DBHome(UpdateHome.this);
                dbh.updateData(Integer.parseInt(e1.getText().toString()),
                        e2.getText().toString(),
                        Integer.parseInt(e3.getText().toString()));
                Toast.makeText(UpdateHome.this, "Data Updated!", Toast.LENGTH_SHORT).show();
                dbh.close();
            }
        });
    }
}
